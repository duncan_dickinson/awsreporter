============
AWS Reporter
============

A demonstration of the `AWS Cache`_ package. The ``reporter.py`` script prepares the cache and processes the
Jinja templates. These produce Asciidoctor files that are then formatted via a docker run.

Requires Python 3 to run the code and Docker for formatting with Asciidoctor.

To prepare the environment you'll need to build the `AWS Cache`_ and install the wheel::

    pip install dist/awscache-0.1.0-py3-none-any.whl

You also need the Jinja 2 package::

    pip install Jinja2

To run the report::

    ./reporter.py --input=demo-input/*.adoc
    docker run --rm -v"$PWD":/documents/ aws-ops-guide asciidoctor -b html5 -v -r asciidoctor-diagram -D output/html output/*.adoc


.. _AWS Cache: https://bitbucket.org/duncan_dickinson/awscache

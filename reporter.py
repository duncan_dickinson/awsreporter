#!/usr/bin/env python3

import argparse
import logging
import os
import shutil
from glob import glob
from os.path import expanduser

from awscache import AwsCache
from jinja2 import Environment, FileSystemLoader


def main():
    parser = argparse.ArgumentParser(prog='AWS Reporter',
                                     description='Uses templates and awscache to generate AWS resource reports',
                                     add_help=True,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-i', '--input',
                        help='The input GLOB to use',
                        required=True,
                        dest='input_glob')

    parser.add_argument('-o', '--output-dir',
                        help='The output directory',
                        default='output',
                        dest='output_dir')

    parser.add_argument('-f', '--database-file',
                        help='The database file to use when caching',
                        default='{}/cache.db'.format(expanduser('~/.awscache')),
                        dest='db_path')

    parser.add_argument('--log',
                        help='The logging level',
                        default='info',
                        dest='loglevel')

    parser.add_argument('--refresh',
                        help='Refresh the locally cached data',
                        action='store_true',
                        dest='refresh')

    args = parser.parse_args()

    loglevel_n = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(loglevel_n, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    logging.basicConfig(level=loglevel_n)

    if not os.path.exists(os.path.dirname(args.db_path)):
        raise ValueError('Check that the directory for the database file exists: {}'.format(args.db_path))

    if os.path.isdir(args.output_dir):
        shutil.rmtree(args.output_dir)
    os.makedirs(args.output_dir)

    cache = AwsCache(args.db_path)

    if args.refresh:
        cache.clear_all()

    cache.load_all()

    fileset = glob(args.input_glob)
    common_dir = os.path.dirname(os.path.commonprefix(fileset))

    if os.path.isabs(common_dir):
        loader = FileSystemLoader('/')
    else:
        loader = FileSystemLoader('./')

    environment = Environment(loader=loader, trim_blocks=True, lstrip_blocks=True)

    for template_file in fileset:
        template = environment.get_template(template_file)
        output_file = os.path.join(args.output_dir, os.path.relpath(template_file, common_dir))
        logging.info('Processing template: {} into {}'.format(template_file, output_file))
        with open(output_file, 'w') as out:
            out.write(template.render(
                {'report': cache.data}
            ))


if __name__ == '__main__':
    main()
